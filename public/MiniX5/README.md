![ScreenShot](ScreenshotMiniX5.png)

RUNME: https://linesdmoller.gitlab.io/ap2020/MiniX5/

Repository: https://gitlab.com/linesdmoller/ap2020/-/blob/master/public/MiniX5/sketch.js

**About my sketch:**

For this miniX I wanted to to redo my miniX3. This was the obvious 
choice for this "revisit the past"-themed miniX, since I couldn't make
my program run the way I wanted to when I made it for the miniX3. 

THIS TIME I FINALLY SUCCEDED! YES!!

The key elements in my program are still the same as in the miniX3; it 
consists of a throbber made as a star being gradually drawn as if it was 
being drawn with a pen on paper. 
Basically, when I made the program for the miniX3, my problem was that I couldn't
figure out how to reset the x- and y-coordinates of the variable "ball" every 
time it was rotated in order to start a new line. Links to my miniX3 are added 
below for comparison... -->

*MiniX3:*
* *RUNME: https://linesdmoller.gitlab.io/ap2020/MiniX3/*
* *Repository: https://gitlab.com/linesdmoller/ap2020/-/blob/master/public/MiniX3/sketch.js*
* *README: https://gitlab.com/linesdmoller/ap2020/-/blob/master/public/MiniX3/READMEMiniX3.md*

...CONTINUED:

After many failed attempts, I finally figured out how to fix that problem! 
Besides fixing this problem, I desicided to make 3 additions to the code:

1. The first of them was part of my original plan for my miniX3, which was that 
I wanted the drawing to reset and start all over again as a traditional throbber 
would. This was done by adding a new background layer, resetting the framecount 
to 0 and resetting the variable "lineNo" to 1 (lineNo: the line number that is 
currently being drawn out of the 8 lines in the star). This addition finished
my intended concept for the miniX3 which was that I wanted to combine the idea 
of a throbber and a progress bar. The animation does this, because the star 
looks like something that has a start and a finish the same way a progress bar 
does. At the same time, the star is gradually drawn in a circle which resembles
a throbber. As expressed in my README for the miniX3, the vague connection 
between a throbber and a progress bar almost makes the user expect a connection
between the actual handling of data and the way the throbber is animated 
visually (as if it was a progress bar). This makes the user expect something
to happen, when the star is fully drawn. But since the throbber now just resets
itself every time the star is fully drawn, it creates tension and further 
emphasizes the meaningless of waiting for data. As expressed in the text, 
Soon, Winnie. "Throbber: Executing Micro-temporal Streams", Computational 
Culture, 2019,; "The loading time of the throbber appears wasted and 
unproductive as it is often associated with the perception of slowness of a 
network." 

2. The second alteration I made was more or less just for fun: I made the lines
of the star be drawn with a "rainbow pen". I did this by simply changing the 
color of the ball/ellipse slightly for every framecount. But this addition is 
not completely useless; it adds an entertainment element to the throbber making
it less boring to look at and thereby affects the perception of time from the 
users point of view - the time flies by more easily.

3. The third thing I added to the throbber was a new element; a white circle 
being gradually drawn around the star. The drawing of the circle finishes
at the same time as the drawing of the star. In this way, the circle further
emphasizes the concept of a progress bar, that has a 'start' and a 'finish'.
But this is not the only function of the circle. 
The "strokeWeigh" of the line in the white circle is defined by
the volume of sound being captured through the user's microphone. This makes the
white circle look like a sound wave when activated. First of all, as with the 
rainbow effect, this alteration adds an element of entertainment for the user. 
Furthermore, this addition also pulls references to the topic of "data capture". 
The effect of this for the user is a contradictory feeling of being entertained 
and being monitored, neglecting privacy in one's own home. *This is kind of fun, 
but... Am I being recorded? What is this recording being used for? Who can hear 
me?*

**Reflection on Aesthetic Programming:**

In the preface of the text, Aesthetic 
Programming: A handbook of Software Studies, (2020 forthcoming), by Soon, W & 
Cox, G., they write about their approax and perspective on the use of aesthetic 
programming in the book; "(It is) an applied and overtly practice-based approach 
to understanding the centrality of programming — the reading, writing and 
thinking with software — as a critical tool for our times, in recognition of the 
way in which our experiences are ever more programmed." This idea of using aesthetic 
programming as a way of sparking critical thinking both for the coder and 
the user can also be said to be true for my program. The throbber is made as an
art piece for the user and the coder to reflect on concepts such as time and 
data capture and how datafication and the evolution of programming has a cultural 
impact. In this way, aesthetic programming can be used to question existing and
new technology in order to act upon those.



