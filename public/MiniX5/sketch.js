//Adding several variables within a variable; the object "ball"
var ball = {
  x: 0,
  y: 100,
  xspeed: -1,
  yspeed: -2.414207124775495 //calculated manually based on laws of triangles
}
var lineNo = 1 // The line currently being drawn in the star
const cir = 135*lineNo; //degrees to rotate, calculated manually based on laws of circles/stars
const frames = 71 //frames pr line, calculated manually based on laws of triangles and then converted into a framecount with help from console.log
let mic;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0)
  frameRate(50)

  // Audio capture
  mic = new p5.AudioIn();
  mic.start();
}
function draw() {
  translate(width/2, height/2);

  // White circle around star
  push()
    let vol = mic.getLevel(); // Get the overall volume (between 0 and 1.0)
    rotate(radians(frameCount/1.577777777777777777+90))
    stroke(255);
    strokeWeight(vol*60)
    fill(255)
    ellipse(140, 0, 5, 5)
  pop()

  //Star
  push()
  if (frameCount > 0 && frameCount < 8*frames) { //when the framcount is between 0 the framecount of a rotation and 8x the framecount (maximum amount of rotations) of a rotation
    if (frameCount > 1*frames && frameCount < 8*frames) { //when the framcount is between 1x the framecount of a rotation and 8x the framecount (maximum amount of rotations) of a rotation
    turn(); //calling function
      if (frameCount > 2*frames && frameCount < 8*frames) { //when the framcount is between 2x the framecount of a rotation and 8x the framecount (maximum amount of rotations) of a rotation
      turn();
        if (frameCount > 3*frames && frameCount < 8*frames) { //when the framcount is above 3x the framcount of a rotation but under the maximum...
        turn();
          if (frameCount > 4*frames && frameCount < 8*frames) { //when the framecount is above 4x the framecount of a rotation...
          turn();
            if (frameCount > 5*frames && frameCount < 8*frames) { // ... 5x framecount of a rotation
            turn();
              if (frameCount > 6*frames && frameCount < 8*frames) {  // ... 6x framecount of a rotation
              turn();
                if (frameCount > 7*frames && frameCount < 8*frames) { // ... 7x framecount of a rotation
                turn();
}}}}}}}}
  move(); //calling function
  colorMode(HSB);
  stroke((frameCount/2) % 360, 100, 100);
  fill((frameCount/2) % 360, 100, 100);

  ellipse(ball.x, ball.y,5,5); //Draw new ball at new position
  pop()
}
//defining move function
function move() {
  ball.x = ball.x + ball.xspeed; //moves ball drawing star
  ball.y = ball.y + ball.yspeed; //moves ball drawing star
      console.log(frameCount/floor(frames)); //This console.log and the one beneath should be the same if the if-statement of "resetBall" should work
      console.log("lineNo " + lineNo);
  if (frameCount/frames == lineNo) { //When the framecount for each rotation has been reached
    resetBall()
  if (lineNo > 8) { //Maximum number of lines in the star has been reached
    resetThrobber()
  }
}
}
//defining rotate function
function turn() {
  rotate(radians(cir)); //rotates the ball drawing the star before it draws a new line
}
//defining function for reset of the position of the ball after each rotation
function resetBall() {
  ball.x = 0
  ball.y = 100
  lineNo = lineNo + 1
//  console.log("resetBall");
}
//defining function for reset of the entire throbber
function resetThrobber() {
  background(0);
  lineNo = 1;
  frameCount = 0;
      console.log("resetThrobber");
}
