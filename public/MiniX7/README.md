![ScreenShot](ScreenshotMiniX7.png)

RUNME: https://linesdmoller.gitlab.io/ap2020/MiniX7/

Repository: https://gitlab.com/linesdmoller/ap2020/-/blob/master/public/MiniX7/sketch.js


For this MiniX I had a hard time comming up with an idea. At first, I thought about 
programming a roulette wheel. I think this would have been a really fun project and 
I actually started coding it but eventually had to give up due to the complexity and
time-consumingness of the project. I believe I could have succeded in making one, if I 
had had the time, but the more I thought about all the details I would have to figure out how to make,
the more I realized, that I didn't have time to put the effort into the project, that I 
wanted to if I were to make this roulette. Perhaps I could have simplified it a bit
but I ultimately came to the conclusion that I didn't want to make it, if I couldn't
fulfill my vision - maybe next time :)


So, my backup idea was perhaps a little bit less creative; a flower generator. My idea was
that I wanted to make a flower with pedals in different sizes, colors and 
positions around the center of the flower. I started out by just making one big flower in the
center of the canvas. After I made this work, I then proceeded by trying
to make several small flowers appear one at a time in random positions of the canvas.
In order not to create a visually confusing giant cluster of flowers, I added 
an if-statement that draws a new background for each 40 flowers to "restart" the drawing on a new "flower field".
My intentetion was to also change the color range for each flower so that the colors of each flower
would be slightly different but the program chose to instead change the color of the 
background - I'm not quite sure what was going on with the code... I also
couldn't figure out how to get all potential hues of the flower pedals. I could either choose
to get the whole range of potential hues for the flower pedal and then also affect the background, 
or I could choose to just go with the yellow/orange flowers and have the intended green-colored background. 
I chose the later. In addition to this, the color of each petals seems to be connected to the 
size of the petal. This was also not intentional but as a matter of fact, I like the restrained yellow color palette  and 
connection between the size of a pedal and its color a lot, although it 
wasn't intentional to make the code run like this. I would guess that it has something to 
do with the way I put the info into the Class, as I am still not confident in using these. 
But as Bob Ross would say; "We don't make mistakes - just happy accidents" :)


The core rules of my program revolve around the random() syntax. The syntax is used 5 times in my code, to generate;
hue of each pedal, rotation of each pedal, x-position of each flower, y-position of each flower and the 
width (and height) of each petal. Over time, my program draws a random "flower field" made up of
randomly generated flowers. The flowers look like the same species of flower;
they are all a similar yellow hue, a similar size and similar a shape. 
You get the impression, that each flower spreads a seed that makes
a new flower sprout in the same way flowers emerge in nature. This makes me realize, that I
might have been able to imitate the laws of nature even better, if the emergence of new
flowers was exponentially increasing. When a flower field is full, the flowers
can travel to other plain fields and reproduce themselves there. This is where the 
if-statement comes into play and a new background is drawn. The green color of the background 
is ofcourse also a reference to the green grass in nature. 


I find the discussion of "what is randomness?" quite interesting. I have never really though
about what rules lie behind something "random". When reading/watching/learning about this 
topic, I realized first of all, that randomness has a psycological impact on people, and that 
this impact has changed over time due to a cultural shift in the perception and assosiations 
of "randomness". In the video assigned for this week Marius Watz talks about "perceived purity
of chance/logic" as one of the criteria for "strong generative art". This can also be linked to 
the concept of "pseudorandomness" which Montfort describes as "the production of random-like values 
that may appear at first to be some sad, failed attempt at randomness, but which is useful and 
even desirable in many cases", in the chapter "Randomness" in "10 PRINT CHR$(205.5+RND(1)); : GOTO 10".
This concept of "pseudorandomness" is the idea of creating structures and rules, that 
seem random due to their complexity and/or lack of trancparency. I also learned, that the syntax
random() is actually based on some complex calculations behind the scenes, which means
that the random() syntax is actually not as random as it is perceived - it is "pseudorandom".
This means, that the randomness of the random() syntax isn't any more or less 
"pure" than if I had tried to come up with a complex calculation myself, that could
also have been perceived as "random". My question is now; is anything actually random? Or is
everything just "pseudorandom" because of its therein contained complex calculations?
When talking about the shift of the mindset towards "randomness" over the decades, Montfort says;
"While in recent days it might be harmless to encounter “a random” sitting in the computer 
lab exploring a system at random, a “random encounter” centuries ago was more likely to
resemble a random encounter in Dungeons & Dragons: a figure hurtling on horseback through 
a village, delivering death and destruction.". The lack of transparency in "randomness" was once
seen as something scary and unpredictable in a bad way. When it comes to the perception
of "randomness" today, Montfort refers to a quote; "As Katie Salen and Eric Zimmerman put it, uncertainty "is a key 
component of meaningful play" (2004, 174). Once the outcome of a game is known, the game 
becomes meaningless. Incorporating chance into the game helps delay the
moment when the outcome will become obvious." This is part of the appeal of "randomness" - 
especially when it comes to games. This perception of "randomness" is also
what I hope people get from my program of the random flower field generator. The
"randomness" in my project is connected to something as harmless and beautiful as flowers, which
minimizes the concequeses of not knowing the outcome. Therefore, the perceivers of my artwork
should be able to lie back and enjoy the emergence of structures - though still with anticipation
and curiosity for the unforeseeable next "move" of the program. 