var petals = []; //array with pedal-objects
var petalNum = 70; //number of petals
var num = 1 //number of flower fields

function setup() {
  createCanvas(windowWidth,windowHeight);
  colorMode(HSB)
  background(120% 360, 30, 255);
}

function draw() {
  frameRate(4)
  //for-loop, that puts the individual rotation of the pedals into the array
  for(var i = 0; i < petalNum; i++) {
    petals[i] = new Petal(random(360), random(floor(255)));
  }
  push()
  translate(random(width), random(height)); //places each new flower at a random position on the createCanvas

  //for-loop that executes all functions in the class on all objects in the array
  for (var j = 0; j < petals.length; j++) {
    petals[j].show();
  }

  //center of flower
  colorMode(RGB)
  noStroke()
  fill(255,235,139)
  ellipse(0, 0, 25)
  pop()

  //Restart drawing when there are 40 flowers on the canvas
  if (frameCount == 40) {
    background(120% 360, 30, 255);
    frameCount = 0
    num = num + 1
  }

  //text "FLOWER FIELD #num"
  textSize(30)
  fill(255)
  textAlign(CENTER)
  text('FLOWER FIELD #' + num, windowWidth/2, windowHeight/12)
}

//class for the petals
class Petal {
  constructor(cir, h) {
    this.rotate = cir
    this.h = h;
    this.s = 150;
    this.b = 255;
    this.w = random(5, 20);
    this.h = 3*this.w
    this.x = 0;
    this.y = this.h/2;
  }

  //function containing information about all the visual elements of the petal
  show() {
    push()
    rotate(radians(this.rotate))
    colorMode(HSB);
    noStroke();
    fill(this.h % 360, this.s, this.b);
    ellipse(this.x, this.y, this.w, this.h);
    pop()
  }
}
