![Screenshot](ScreenshotMiniX6.png)

RUNME: https://linesdmoller.gitlab.io/ap2020/MiniX6/

Repository: https://gitlab.com/linesdmoller/ap2020/-/blob/master/public/MiniX6/sketch.js

My program is made as a comment on the "hoarding-on-toilet-paper" situaion due to the pandemic of corona virus.
Wednesday, the prime minister, Mette Frederiksen, announced that Denmark would be "shut down" due to the rappit spreading of corona virus.
This announcement sparked panic among the citizens and people started flocking to the supermarkets in order to stock up on everything needed for 
a complete meltdown of the social infrastructure and at least 14 days (probalby more) of non-stop quarantine. 
For some reason, one thing people particularly made sure to stock up on was toilet paper. Despite Mette F. saying that this wasn't
a food crisis and there would be more than enough goods for everyone if we just proceeded to shop as we normally would, people were willing
to fight over toilet paper.

My initial idea for this program was to make a catching-game similar to Winnie's tofu game, just with toiletpaper falling from the top to the bottom instead.
As you might be able to tell though, this is not what I ended up making, since I wanted to explore other ways of making games besides the way Winnie did it. 
So I started researching on simple games I could make using p5.js and I stubbled across the classic "ping pong"-game, where an object (usually a ball)
goes from one side of the canvas to the other and you have to bounce it back with some sort of bat or wall. 

My program works just like the classic "ping pong"-game. The only difference is that you use shopping carts as bouncers/bats/walls and the ball is a toilet paper roll.
To keep count of your score, there are two counters at the top of the canvas - one GREEN counter is called "Toilet paper all for yourself" which counts
the amount of times you have bounced the toilet paper with one of the shopping carts. The other RED counter is called "Toilet paper lost to other customers" and
counts the amount of times you have missed the toilet paper with the shopping carts and the toilet paper roll. 

In order to make this program, I had to download the
p5.play library. I found it a little bit confusing to work with this library in the beginning but luckily, I figured it out. 
Other parts of the coding were also quite tricky - for example, besides making sprites for the bats (shopping carts), 
I had to make the top and the bottom of the screen into sprites as well in order to make the ball (toilet paper) bounce back from those.

Object oriented programming can be used in many clever ways. First of all, using this aproach makes the coding process simpler. If you have to code
a significant amount of objects with more or less the same data and functionalities is both much faster and much easier to read if you categorise these
into classes/objects. By doing this, you can simply use the class as a mold for however many iterations you want to make of the same thing. This also adds to the
usability aspect of object oriented programming. Another advantage of using this approacch is, that it becomes much faster and easier to change 
parameters of all iterations of an object at the same time instead of changing the same code multiple times in different locations of the 
bigger code. One thing about using object oriented programming is the cultural impact of this. By using objects/classes, you pick and choose which aspects
of an element/object you define as essential for the interpretation of this. This is what you could refer to as the "object abstraction".
Every new object fits within the same "mold" difined by a subjective interpretation of what is essential for this object. 
When something is subjective, it will always change the cultural impact.

I have thought about some things, I could do better in my program:
After looking at the backgroung image I picked, I realised that it would have been fun to make a counter for each time the toiletpaper hit 
the big shopping cart in the bottom of the screen instead of the two small ones. This would have been a fun twist to the classic version
of a "ping pong"-game. If I were to do this, I would also have to change the "bats" from being two small shopping carts to being two hands - one left and one right.
This change could perhaps also help eliminate another bug I have found in my program; I realized that the toiletpaper somethimes gets stuck over the small shopping
cart and makes the green counter count a few too many times. This is possibly due to the irregular shape of the shopping cart.
