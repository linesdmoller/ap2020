var cartLeft, cartRight, toiletpaper, windowTop, windowBottom, swing;
var score1 = 0; //score starts at 0
var score2 = -1; //score starts at 0

//class for the game objects
class gameObject {
  constructor(size, image, speed){
    this.size = size;
    this.image = image;
    this.speed = speed;
  }
}
//game objects with definition of size, image and speed
var cart = new gameObject(80, "./data/cart.png");
var toiletpaper = new gameObject(100, "./data/toiletpaper.png", 3);

//PRELOAD
function preload() {
  img1 = loadImage(toiletpaper.image);
  img2 = loadImage(cart.image);
  img3 = loadImage("./data/supermarket.jpg");
}

//SETUP
function setup() {
  createCanvas(750, 562);

  cartLeft = createSprite(cart.size, height/2); //position
  cartLeft.addImage(img2);
  cartLeft.immovable = true; //toilet paper bounces back and can't move the left cart

  cartRight = createSprite(width - cart.size, height/2);
  cartRight.addImage(img2);
  cartRight.immovable = true; //toilet paper bounces back and can't move the right cart

  windowTop = createSprite(width/2, -30/2, width, 30);
  windowTop.immovable = true; //toilet paper bounces back and can't move the sprite

  windowBottom = createSprite(width/2, height+30/2, width, 30);
  windowBottom.immovable = true; //toilet paper bounces back and can't move the sprite

//toilet paper roll sprite with image and maxSpeed
  roll = createSprite();
  roll.addImage(img1);
  roll.maxSpeed = 30;
}

//DRAW
function draw() {
  img1.resize(toiletpaper.size, toiletpaper.size);
  img2.resize(cart.size, cart.size);

//background
  image(img3, 0, 0, 750, 562);

//y-coordinates of the carts
  cartLeft.position.y = constrain(mouseY, cartLeft.height/2, height-cartLeft.height/2);
  cartRight.position.y = constrain(mouseY, cartRight.height/2, height-cartRight.height/2);

//make the toiletpaper bounce off the top and bottom of the screen
  roll.bounce(windowTop);
  roll.bounce(windowBottom);

//when the toilet paper hits the left cart
  if(roll.bounce(cartLeft)) {
    toiletpaper.speed = toiletpaper.speed + 0.25;
    swing = (roll.position.y - cartLeft.position.y) / 3;
    roll.setSpeed(toiletpaper.speed, roll.getDirection() + swing);
    score1++; //increase green value: toilet paper for yourself

  }

//when the toilet paper hits the right cart
  if(roll.bounce(cartRight)) {
    toiletpaper.speed = toiletpaper.speed + 0.25;
    swing = (roll.position.y - cartRight.position.y) / 3;
    roll.setSpeed(toiletpaper.speed, roll.getDirection() - swing);
    score1++; //increase green value: toilet paper for yourself
  }

//make the toilet paper roll reappear in the middle when it is not caught by the left cart
  if(roll.position.x < 0) {
    roll.position.x = width / 2;
    roll.position.y = height / 2;
    toiletpaper.speed = 3;
    roll.setSpeed(toiletpaper.speed, 0);
    score2++; //increase red value: toilet paper for others
  }

//make the toilet paper roll reappear in the middle when it is not caught by the right cart
  if(roll.position.x > width) {
    roll.position.x = width / 2;
    roll.position.y = height / 2;
    toiletpaper.speed = 3;
    roll.setSpeed(toiletpaper.speed, 180);
    score2++; //increase red value: toilet paper for others
  }

//score count
  noStroke();
  textSize(25);
  fill(0, 255, 0)
  text("Toilet paper all for yourself: " + score1, 200, 30);
  fill(255, 0, 0);
  text("Toilet paper lost to other customers: " + score2, 150, 60);

  drawSprites();
}
