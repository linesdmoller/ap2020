//Adding several variables within a variable; the object "ball"
var ball = {
  x: 0,
  y: 100,
  xspeed: -1,
  yspeed: -2.414207124775495 //calculated manually based on laws of triangles
}
var lineNo = 1
var cir = 135*lineNo; //angle to rotate calculated manually based on laws of circles/stars
var yline = 70.711

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0)
  frameRate(50)
}
function draw() {
  translate(width/2, height/2);
    if (frameCount > yline && frameCount < 8*yline) {  //calculated manually based on laws of triangles
    turn();
    lineNo = lineNo + 1 //keeping count of the number of lines drawn in the star
      if (frameCount > 2*yline && frameCount < 8*yline) {  //calculated manually based on laws of triangles
      turn();
      lineNo = lineNo + 1 //keeping count of the number of lines drawn in the star
        if (frameCount > 3*yline && frameCount < 8*yline) {  //calculated manually based on laws of triangles
        turn();
        lineNo = lineNo + 1 //keeping count of the number of lines drawn in the star
          if (frameCount > 4*yline && frameCount < 8*yline) {  //calculated manually based on laws of triangles
          turn();
          lineNo = lineNo + 1 //keeping count of the number of lines drawn in the star
            if (frameCount > 5*yline && frameCount < 8*yline) {  //calculated manually based on laws of triangles
            turn();
            lineNo = lineNo + 1 //keeping count of the number of lines drawn in the star
              if (frameCount > 6*yline && frameCount < 8*yline) {  //calculated manually based on laws of triangles
              turn();
              lineNo = lineNo + 1 //keeping count of the number of lines drawn in the star
                if (frameCount > 7*yline && frameCount < 8*yline) {  //calculated manually based on laws of triangles
                turn();
                lineNo = lineNo + 1 //keeping count of the number of lines drawn in the star
}}}}}}

//    if (framecount/floor(yline) == lineNo) {
//    ball.x = 0
//    ball.y = 100
//}
  move();
  stroke(255)
  ellipse(ball.x, ball.y,5,5);
}
//defining move function
function move() {
  ball.x = ball.x + ball.xspeed;
  ball.y = ball.y + ball.yspeed;
}
//defining rotate function
function turn() {
  rotate(radians(cir));
}
}
