RUNME: https://linesdmoller.gitlab.io/ap2020/MiniX3/

Sketch: https://gitlab.com/linesdmoller/ap2020/-/blob/master/public/MiniX3/sketch.js

**DISLAIMER:** 
*I couldn't figure out how to reset the x- and y-coordinates for the variable 
"ball". I tried to redefine ball.x and ball.y within the if-statements but I
realized by doing that, that this would make the ball/line stop 
moving and just draw a static ball. For this reason, every time the line/ball 
is rotated the new line starts with the updated x- and y-coordinates from
the previous line. I tried at least 3782 solutions to fix this problem but
eventually I had to give up.*

**About my sketch:**

For this MiniEx I wanted to code a star that would be drawn gradually as if 
it was being drawn with a pen on paper. First of all, with this piece I wanted 
to challenge myself to code something with a very specific set of measurements.
This required of me that I considered carefully how to apply the animation 
aspects of the sketch in order to end up with the intended icon.

I am proud that I figured out exactly how much the lines had to be rotaded and
how much should be added to x and y of the ball in order for it to move in 
a specific direction with a specifik angle from the base. Something else I had
to figure out was how to define the point at which each rotation should happen.
At first I tried to define the maximum/minimum y-coordinate for the ball. This
coordinate was also calculated manually, and I believe it would have worked 
if I had figured out how to reset the x- and y-coordinates of the ball.
Instead, I ended up using the framecount to define when the rotation should
happen. I used *print()* to tell me in the console log at what framecount 
the maximum y-coordinate from before had been reached and then I just added
that framecount to the if-statements.

If I had figured out how to reset the x- and y-coordinates of the ball I 
would have liked to also make the entire drawing of the star reset at the end.

The idea behind my sketch was that I wanted to play with the concept of 
combining a throbber and a progress bar. The animation of the star being drawn
could be perceived as something that has a start and a finish. Therefore, 
The animation sparks anticipation and the user/observer almost expects 
something to happen when the star is fully drawn. This does not happen and - if 
I had coded what I intended to - the star is just reset and drawn again. 
The vague connection between the throbber and the progress bar makes the 
user believes that there is a connection between the handling of data 
and how the throbber is animated visually but as expressed in the readings for
this week; "The actual handling of data does not have a direct relation with 
how the throbber is animated visually.". This creates tension and further
emphasizes the meaningless waiting - especially after the first iteration of
the star. As expressed in the text; "The loading time 
of the throbber appears wasted and unproductive as it is often associated with 
the perception of slowness of a network.".
I used the framerate and framecounts as the time elements in the throbber. 
By doing this, there was no connection between the handling of the data and 
the animation itself. Furthermore, the framerate is depended on the speed of
each device using it. This creates variation in the perception of the throbber
and the idea of time. I find this interesting since the throbber is therefore, 
still depended on the streamming speed of data.
