Final Project made in collaboration with Linnea Aandahl, Laura Kirstine Schulz and Herborg Kjærbæk

RUNME: https://herborg.gitlab.io/ap-2020/FinalProject/

Repository: https://gitlab.com/Herborg/ap-2020/-/tree/master/public/FinalProject

Sketch: https://gitlab.com/Herborg/ap-2020/-/blob/master/public/FinalProject/sketch.js
