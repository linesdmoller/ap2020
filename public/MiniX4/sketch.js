let ctracker;
let mic;

function setup() {
  background(100);
  // Audio capture
  mic = new p5.AudioIn();
  mic.start();

  //web cam capture
  let capture = createCapture();
  capture.size(640,480);
  capture.position(0,0);

  //setup face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);
}
function draw() {
  //capture.hide();
  let c = createCanvas(640, 480);
  c.position(0,0);

  let vol = mic.getLevel(); // Get the overall volume (between 0 and 1.0)
  let positions = ctracker.getCurrentPosition();
  if (positions.length) { //check the availability of web cam tracking
    //getting the audio data
      if (vol > 0.01) {
        //drawing the red crosses on eyes
        stroke(255, 0, 0)
        strokeWeight(10)
        line(positions[27][0]-20, positions[27][1]-20, positions[27][0]+20, positions[27][1]+20)
        line(positions[27][0]-20, positions[27][1]+20, positions[27][0]+20, positions[27][1]-20)
        line(positions[32][0]-20, positions[32][1]-20, positions[32][0]+20, positions[32][1]+20)
        line(positions[32][0]-20, positions[32][1]+20, positions[32][0]+20, positions[32][1]-20)
        //drawing "CENSORED" on mouth
        noStroke()
        strokeWeight(1)
        fill(0)
        rect(positions[60][0]-60, positions[60][1]-20, 120, 40)
        textSize(20)
        fill(255)
        text('CENSORED', positions[60][0]-60, positions[60][1]+10)
    }
}
}
