![Screenshot](ScreenshotMiniX4.png)


RUNME: https://linesdmoller.gitlab.io/ap2020/MiniX4/

Sketch: https://gitlab.com/linesdmoller/ap2020/-/blob/master/public/MiniX4/sketch.js

**Title of artwork:** 

"The trinity of power; 3x code"

**Description:**

*Code Is reliable. Code makes no human errors. Code captures all.

Capturing data in code makes an undiscussable processing of rules. Moreover, 
with the development of new technology comes increasing possibilities of 
expanding and finetuning the amount and quality of the data being captured. 
Code is already a highly sophisticated tool and it is only a matter of time 
before code will be able to differentiate and analyze extremely complex 
real-life scenarios. 

So for what reasons should code NOT be implemented as a central part of the 
legal system of society?

The trinity of power – the legislative, the judicial and the executive – is the 
foundation of our society. Through code and the implementation of data capture, 
the trinity will become one! Code makes the laws, code interprets the law, and 
code enforces the laws. One big reliable SUPERPOWER! 

But what happens, when the voice is taken from the people?*

**Reflection on the assignment:**

For this assignment I wanted to create an artwork that criticizes a dystopia 
or should i say possible future, where we have en extreme reliance on code and 
data capture as a way of keeping law an order in a society.

I chose to make a program, with several data capture aspects: the program 
catches you on camera, analyses the position of your face and catches the 
volume level of sound being detected through your microphone. 

The idea was to question the possibilities that come with the developement of 
new surveiliance techniques and technology. Of course, there are many pros
in the form of improved abilities to catch criminals and thereby creating
a safer world to live in. This can already be seen in the way automatic speeding
cameras capture photos, license plates and the exact speed at which the car is 
going and aotumatically sends the ticket to the reciever. But there are also 
many cons. The con, I was trying to explore in this artwork was the possible 
threatening of freedom of speech. I expressed this by drawing red crosses over 
the eyes and writing 'CENSORED' over the mouth of the user every time sound was 
detected through the microphone.

The artwork also makes the user reflect on the rules, that controle it -> the 
syntax. Søren Pold talks a bit about this in the text; Pold, Søren; "Button", 
Matthew Fuller, ed. Software Studies (Cambridge, Mass.: MIT Press 2008).
He writes;

"The functional spell is only broken when the software crashes, or when the 
software becomes reflexive: either through artistic means as in net- and 
software art, in order to surprise, criticize, or inform; or through juridical 
necessities such as when submitting to licenses, etc."

Pold talks about how the illusion of a mechanical button can be destroyed and
make the user reflect on the code behind - either when the software crashes
or when the software is made as an artwork.
In this case, it is not the illusion of a mechanical button that is being 
destroyed; it is the illusion of control.
