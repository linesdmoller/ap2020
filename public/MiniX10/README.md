**Revisit the past:**

For the individual assignment of revisiting an older MiniX and making a flowchart for it, I have chosen
to look at my MiniX 8 since this was the one where we had to use JSON files which required a new way of thinking 
in terms of the chronology in code. Furthermore, this was also a somewhat long and complex code.

RUNME: https://linesdmoller.gitlab.io/ap2020/MiniX8

Repository: https://gitlab.com/linesdmoller/ap2020/-/blob/master/public/MiniX8/sketch.js

For this flowchart, I chose to focus on making a flowchart that describes how the code technically runs. 
My aim was to describe it in way that would hopefully be understandable for both someone with absolutely no knowledge of coding and also 
adequate enough for a coder to understand the general technical procdure of coding and running the code.

![Screenshot](flowchart_MiniX8.png)

*NOTE: I have realised that I should have perhaps also made diamond shapes around the "should the left and right button be hidden or shown?"-sections 
of the flowchart and then made the actions of "hide" and "show" depending on the answer of wether or not the page number is at its maximum/minimum.*


**Ideas for the final project:**

*This Mini Ex is made in collaboration with Herborg Hjartvardsdóttir Kjærbæk, Laura Kirstine Schulz and Linnea Aandahl*

1.  Air Pollution and covid19

One of our ideas for the final project came from the topical discussion on covid19's (the lock down's) positive impact on the environment. 
Therefore, the idea is to make a program that suggests an inversely proportional connection between the amount of 
air pollution and the amount of covid19 cases in individual countries. Specifically, we would like to display 2 vertical bars - one 
to indicate the amount of covid19 cases in a given country (percentage), and one to indicate the amount of air pollution in that country (percentage). 
The program should have the option of clicking through different countries to see the data from this country displayed in the 2 bars.
The data for the 2 bars will come from API('s).
For this flowchart, we chose to focus on creating one that descibes how the program runs visually. 

![Screenshot](flowchart_air_pollution_vs_covid19.png)

2.  Covid19 quiz

Another one of our ideas for the final project actually came from the general structure of a flowchart itself (...and covid19 of course). 
Our idea is to create a quiz that will ask you questions on your health and other covid19 related questions in order to give the 
user advice on how to handle the crisis responsibily. Specifically, the program will display one multiple choice question at a time 
followed by some predefined answers (buttons) for this question. Depending on the user's answer for this question, 
the program will present the user with an elaborate question for the previous question. 
For this flowchart as well as for the previous one, we also chose to focus on creating one that descibes how the program runs visually. 
Somewhere along the line though, this flowchart ended up being more of a sketch for the connection between the individual questions. 
Fortunately, we feel like this flowchart gives a great overview of the concept of our idea, since there is a strong connection
between how the program runs and how a flowchart "runs".

![Screenshot](flowchart_covid19_quiz.png)


* What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure? 

When writing a flowchart at the level of algorithmic procedure (code), the flowchart will describe in detail specifically 
how the code is read by the computer and runs. This is a relevant way of making a flowchart between coders if their aim is to 
explain the code to one another down to each technical procedure. 
In "The Multiplicity of Algorithimcs" in If...Then: Algorithmic Power and Politics, Taina Bucher writes: 
> "From a technical standpoint, creating an algorithm is about breaking the problem down as efficiently as possible, which implies a careful planning of the steps to be taken and their sequence." (Bucher 2018, p. 6)

As Bucher explains, algorithms are great for breaking down a problem and forcing the coder to reflect on the exact execution of the coding. 
But people with no experience with coding have no chance of understanding a flowchart created with this approach.
The reason why it might be relevant to choose another approach for the flowchart is first of all that the flowcharts can be used by the coders
to plan and communicate loose concepts to one another and as a tool for critical thinking. Second of all, the flowcharts can be 
used to communicate between coders and non-coders (fx clients and business partners). Therefore, it is relevant to prectice both approaches 
to making a flowchart and sometimes even combining the two. 

The challenge when it comes to combining the two approaches might be, that there is a gap between the coders and the non-coders, that needs 
to be filled. The two approaches to making a flowchart vary a lot in both the sequence of operations and the language. Therefore, 
in order to make a flowchart that accomodates both backgrounds, the flowchart will have to land somewhere in the middle. The problem is then 
that neither the coder nor the non-coder, will get the full anvantages of reading the flowchart - it will always be a compromise.

* What are the technical challenges for the two ideas and how are you going to address them?

For the first idea (Air pollution and covid19), the data for the 2 bars - air polution and covid19 cases - would most likely have to come from 
2 seperate API's. This means, that we might get in trouble when trying to find 2 comparable API's in terms of their structure 
(countries, measurements, etc.). Futhermore, we wil have to figure out how to best display comparable data in the two bars. Our idea 
is to do this by converting the values for each bar into some sort of percentage measurements. Our initial idea was to also display a change 
over time, but this would probalby be too complicated, since the API's will most likely only give us the current values, and we would also 
have to figure out how to make a graph of some sort (if we are not lucky enough to find an API that includes a graph). Specifically, we would 
like to also indicate the air pollution level BEFORE covid19 in the given country as a standard to compare with the current level. This we 
would also need to figure out how to do in the best way. 

For the second idea (covid19 quiz), I think one of the main challenges we will have to figure out is how the navigate and direct the program 
to display a specific question and answer-buttons depending on the combination of answers given by the user. This will probably involve a bunch of if-statements. 

* What is the value of the individual and the group flowchart that you have produced?

The three flowcharts are all different. The group flowcharts for the two ideas for the final project are similar in the sence that they are 
written as an explanation of the visual procedures of the program and that they are written more for the user than the flowchart for my 
old miniX. Yet they are different in the way they "explain" the visuals - the first one explains the visuals more concretely whereas the 
second one explains the pattern of questions in a more focused version. Another thing these two flowcharts have in common is, that they made 
us reflect on how we want our programs to run. Among other things, we started reflecting on the visuals - specifically how the data in the air 
pollution program would be displayed in a usable and aestetic way. This is why we started thinking of converting the values into percentages.
The flowchart for own miniX was written AFTER the program had been coded. Nevertheless, I found that making the flowchart was still 
a great tool for me to reflect on my own code and reevaluate it. I found that some of the code I had written could be tidyed up a little. 
The flowchart for this project is more detailed and focussed on explaining the way the code is written and technically executed by the computer. 
It is written in a language and with an amount of detail that would hopefully both be usefull to a programmer who was to understand it (and 
maybe copy) and to someone with no prior experience with coding. 
