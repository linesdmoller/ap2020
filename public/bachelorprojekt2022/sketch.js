//GLOBAL VARIABLES:
let bubbles = [];  //Empty array being filled by Bubble Class parameters later
let bubbleColor = ["lightgreen", "darkseagreen", "yellowgreen",
"mediumseagreen", "mediumspringgreen", "greenyellow",
"olivedrab", "forestgreen", "lawngreen", "lime", "darkgreen", "olivedrab",
"lightseagreen", "olive", "darkolivegreen"] //HTML/CSS styling colors
let index;
let dictionary;
let mostFrequentWords = [];
//empty variables for txt and json file loading later:
let stopwordsDa;
let stopwordsEn;
let brainstorming;


//PRELOAD CODE:
//Any asynchronous data loading in "preload" is completed before "setup" is run.
function preload() {
  //load json files containing stopwords in English and in Danish
  stopwordsDa = loadJSON('da_stopwords.json');
  stopwordsEn = loadJSON('en_stopwords.json');
  //load txt file containing the sentences from the brainstorming session
  brainstorming = loadStrings('brainstorming.txt');
}


//SETUP CODE:
//Code in "setup" is only run once and will finish before running "draw".
function setup() {
  frameRate(500); //Ups the framerate to 500/sek - the standard is 60/sek.

  let sentences = brainstorming; //Array of the sentences from brainstorming

  console.log("### Sentences ###");
  console.log(sentences);

  //Load Danish stopwords into an array:
  let daStopWords = [];
  for (let i in stopwordsDa){
    daStopWords.push(stopwordsDa[i]);
  }
  //Load English stopwords into an array:
  let enStopWords = [];
  for (let i in stopwordsEn){
    enStopWords.push(stopwordsEn[i]);
  }

  console.log("### Stopwords ###");
  console.log("# Danish stopwords #");
  console.log(daStopWords);
  console.log("# English stopwords #");
  console.log(enStopWords);

  //Concatenate the arrays of Danish and English stopwords into one array:
  let stopWords = daStopWords.concat(enStopWords);

  //Preprocess the sentences:
  let processedSentences = processSentences(sentences, stopWords); //calls function "processSentences" and inserts parameters.
  console.log("### Processed sentences ###");
  console.log(processedSentences);

  //Get all the keywords and their individual frecuencies/weight:
  dictionary = getDic(processedSentences); //calls function "getDic" and inserts parameter "processedSentences".
  console.log("### Vocab and frecuencies ###");
  console.log(dictionary);

  //Rank the sentences by the average of the weight of the words:
  let rankedSentences = rankSentences(sentences, processedSentences, dictionary); //calls function "rankSentences" and inserts parameters.
  rankedSentences.sort(compare);
  console.log(rankedSentences);

  //Choose the top 15 sentences:
  console.log("### top 15 sentences ###");
  let n = 15;
  rankedSentences = rankedSentences.slice(0, n);
  console.log(rankedSentences);

  //Prepare canvas:
  createCanvas(windowWidth, windowHeight)
  //  console.log(windowWidth, windowHeight);
  let assignedColors = {}; //Empty dictionary for keyword and color pairs
  for(var i = 0; i < n; i++) {
    index = rankedSentences[i]["weight"];
    let x = random(width);
    let y = random(height);
    let r = index*25;
    let keyWord = rankedSentences[i]["fattestWord"];  //Take the word with the highest weight and save it as a keyWord.

    let color = "";
    if (keyWord in assignedColors) { //If the keyWord already has an assigned color.
      color = assignedColors[keyWord]; //Assign that color to the bubble.

    } else { //If the keyWord doesn't already have an assigned color.
      color = bubbleColor.shift(); //Assign a new color to the bubble.
      assignedColors[keyWord] = color; //Add the new keyWord-color pair to the dictonary
    }

    let oneLineSentence = rankedSentences[i]["sentence"]; //Sentence without line breaks.
    let fontSize = r / 4;
    let text = splitSentences(oneLineSentence, r, fontSize); //Add line breaks to sentences so they fit in their bubble.
    bubbles[i] = new Bubble(x, y, r, color, text, fontSize); //Parameters for a new bubble.
  }

  //Find the most frequent words and their colors:
  for (let word in assignedColors) {
    let color = assignedColors[word];
    let weight = dictionary[word];
    mostFrequentWords.push({"word": word, "color": color, "weight": weight})
  }
  //Sort the most frequent words and their colors:
  console.log(mostFrequentWords);
  mostFrequentWords.sort(compare);
  console.log(mostFrequentWords);
}

//DRAW CODE:
//Code under "draw" loops forever (unless told otherwise).
function draw() {
  background("limegreen") //HTML/CSS styling color

  //Top 7 keyWord list displayed on the left side of the screen:
  fill(0); //Color black.
  rect(10, 10, 90, height - 20); //Plots the black frame underneath the list.

  if (mostFrequentWords.length > 7) {
    mostFrequentWords = mostFrequentWords.slice(0, 7) //Plot only the 7 most frequent words.
  }
  for (var a = 0; a < mostFrequentWords.length; a++) {
    push();
    let rectSpacing = (height - 30) / 7
    fill(mostFrequentWords[a]["color"]); //Color of each reactangle matches the color assigned to the keyWord.
    rect(20, a*rectSpacing + 20, 70, (rectSpacing - 10)); //Draw the small rectangles
    fill(0); //Black color.
    textSize(10);
    textStyle(BOLD);
    textAlign(CENTER);
    let listNo = a + 1 + "." //Numbering the keywords
    text(listNo, 55, a*rectSpacing + ((rectSpacing + 40)/2) - 7) //Plot numbers on list.
    let txt = mostFrequentWords[a]["word"][0].toUpperCase() + mostFrequentWords[a]["word"].slice(1);
    text(txt, 55, a*rectSpacing + ((rectSpacing + 40)/2) + 7); //Plot the 7 most frequent words.
    pop();
  }

  //Check if the randomly placed bubbles are overlapping the window border or each other:
  for (var i = 0; i < bubbles.length; i++) {
    let overlapping = false;

    //Check if bubbles overlap window borders:
    if (bubbles[i].x > (width - bubbles[i].r - 10) || bubbles[i].x < (bubbles[i].r + 110) ||
    bubbles[i].y > (height - bubbles[i].r - 10) || bubbles[i].y < bubbles[i].r + 10) {
      overlapping = true;
    }
    //Check if bubbles overlap each other:
    for (var j = 0; j < bubbles.length; j++) {
      if (i != j && bubbles[i].intersects(bubbles[j])) {  //Add conditions about the window borders later
        overlapping = true;
      }
    }
    //If the bubbles are overlapping anything, give them a new random position until they fit.
    if (overlapping) {
      bubbles[i].changeCoordinates();
    } else {
      bubbles[i].show();
    }
  }
}

function removeStopWords(words, stopWords) {
  //Remove stopwords (a, the, ...) from the string
  let notStopWords = [];
  for (let word of words) {
    if (!stopWords.includes(word)) {
      notStopWords.push(word);
    }
  }
  return notStopWords;
}

//Preposess sentences:
function preprocessSentence(str, stopWords) {
  str = str.replace(/[^\w\s]|_/g, "").replace(/\s+/g, " "); //Remove puntuation marks from string
  str = str.toLowerCase(); //Make string lowercase
  let words = str.split(" "); //Split sentence into words
  words = removeStopWords(words, stopWords); //Remove stopwords
  return words;
}

//Process sentences:
function processSentences(sentences, stopWords) {
  let clean_sentences = []; //Empty array
  for (let sentence of sentences) {
    let words = preprocessSentence(sentence, stopWords); //Calls function above
    clean_sentences.push(words);
  };
  return clean_sentences;
}

//Get all the keywords and their individual frequencies:
function getDic(sentences) {
  let vocabSize = 0;
  let dic = {};
  for (let sentence of sentences) {
    for (let word of sentence) {
      vocabSize++;
      if (word in dic) {
        dic[word] = dic[word]+1;
      } else {
        dic[word] = 1;
      };
    };
  };
  for (let key in dic){
    dic[key] = dic[key] / vocabSize * 100;
  }
  return dic;
}

//Compare weights:
function compare(a, b) {
  return b["weight"] - a["weight"];
}

//Rank sentences by the average weight of the words:
function rankSentences(sentences, processedSentences, dictionary) {
  let rankedSentences = []; //Empty array
  for (let s in sentences) {
    let totalWeight = 0;
    let maxWeight = 0;
    let fattestWord = "";
    for (let word of processedSentences[s]) {
      let weight = dictionary[word];
      totalWeight += weight;
      //Find the word with the highest weight:
      if (weight > maxWeight) {
        maxWeight = weight;
        fattestWord = word;
      }
    }
    let len = processedSentences[s].length;
    rankedSentences.push({"sentence": sentences[s], "words": processedSentences[s], "weight": Math.sqrt((totalWeight/len)), "fattestWord": fattestWord, "weight_mult": 1})
  }
  return rankedSentences;
}

//Split sentences to make them fit within the borders of their own bubble:
function splitSentences(sentence, bubbleR, fontSize) {
  const maxCharactersInLine = 14;

  let wordsWithNewlines = []; //Empty array.
  let currentCharactersInLine = 0;

  for (let word of sentence.split(" ")) { //For each word in sentence.
    let wordLenght = word.length;

    if (currentCharactersInLine + wordLenght > maxCharactersInLine) { //If current word doesn't fit.
      if (wordsWithNewlines.length > 0) { //If array isn't empty (I don't want a \n character in the beginning).
        wordsWithNewlines.push("\n"); //Insert newline.
      }
      wordsWithNewlines.push(word); //Add word to array after newline character.
      currentCharactersInLine = wordLenght + 1; //+1 for the blank space between words.
    } else {
      currentCharactersInLine += wordLenght +1; //+1 for the blank space between words.
      wordsWithNewlines.push(word); //Add word to array.
    }
  }
  let splitedSentence = wordsWithNewlines.join(' '); //Join array of strings.
  return splitedSentence;
}

//Saves paramters for each bubble:
class Bubble {
  constructor(x, y, r, color, text, fontSize) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.color = color;
    this.text = text;
    this.fontSize = fontSize;
  }

  //Checking if the distance between the center of 2 bubbles is smaller than the sum of the radiuses.
  intersects(otherBubble) {
    let d = dist(this.x, this.y, otherBubble.x, otherBubble.y);
    return d < this.r + otherBubble.r;  //Returns "true" or "false".
  }

  //New random coordinates (function called if bubbles are overlapping):
  changeCoordinates() {
    this.x = random(width);
    this.y = random(height);
  }

//Plot the bubbles and sentences:
  show() {
    noStroke();
    fill(this.color);
    let bubble = ellipse(this.x, this.y, this.r * 2);
    textSize(20);
    textSize(this.fontSize);
    fill(0);
    textAlign(CENTER);

    let numberOfLines = this.text.split("\n").length; //Number of lines in splited sentence.
    let deltaY = 0; //DeltaY 0 by default (for 1 line).
    if (numberOfLines > 1) { //If more than one line.
      if (numberOfLines % 2 == 0) { //If even number.
        deltaY = ((numberOfLines / 2) - 0.5) * this.fontSize; //Number of lines above center - 0.5 (for the first line being above the center)* textSize.
      } else { //If odd (other than 1).
        deltaY = ((numberOfLines - 1) / 2) * this.fontSize; //Number of lines above center * textSize.
      }
    }
    text(this.text, this.x, this.y - deltaY); //Y = this.y - deltaY.
  }
}
