'SenCloud'

Dette er mit konstruerende element, udarbejdet ifm. mit bachelorprojekt i Digital Design, E21.

Link til rendering af programmet:

RUNME: https://linesdmoller.gitlab.io/ap2020/bachelorprojekt2022/

Obs.: Programmet er testet og kører bedst i en Chrome browser.

Sketch: https://gitlab.com/linesdmoller/cultural-data-science/-/blob/main/bachelorprojekt/sketch.js

Sammen med programmet er der uploadet en txt-fil med pseudo-data, dvs. et opdigtet datasæt, der er brugt til at teste programmet.
