var img;

function preload(){
	img = loadImage("./data/skattejagt.jpg");
}

function setup() {
  var cnv = createCanvas(windowWidth, windowHeight);
  cnv.style('display', 'block');
  background(0);
}

function draw() {
  frameRate(100000)
  // get the colour value from the image
  // at the mouse location
  var paint = img.get(mouseX,mouseY);
  // colour is an array like so: [r,g,b,a]
  // we can set the alpha value like this:
  paint[3] = 256;
  fill(paint);
  noStroke()
  var diam = random(2,8); // random diameter
  ellipse(mouseX,mouseY,diam,diam); //draw the ellipse

}
