
RUNME: https://linesdmoller.gitlab.io/ap2020/MiniX2/
Sketch: https://gitlab.com/linesdmoller/ap2020/-/blob/master/public/MiniX2/sketch.js

**Describe your program and what you have used and learnt.**

In this mini ex i explored conditional structures. I chose to create a happy
purple smiley face and a sad purple smiley face that I could switch between
by pressing the mouse. By choosing to make these smileys I also got to 
explore how to make arces in p5.js. This was a challenge at first and I
spend some time trying to propberly understand how to use the syntax but
I figured it out.


**How would you put your emoji into a wider cultural context that concerns 
representations, identity, race, social, economics, culture, device politics 
and beyond? (Try to think through the assigned reading and your coding process,
and then expand that to your experience and thoughts - this is a difficult 
task, you may need to spend sometimes in thinking about it).**

After reading/watching the assigned readings/videos for this week, I realized
first of all that because emojis are becomming more and more "lifelike" there
is a rising demand for more diversity and options when it comes to emojis.
Some of the dilemmas in the lack of diversity refer to things such as
gender, sexuality, race, skintone and politics. 
This goes to a point where there is almost no turning back. But this
demand for diversity has only started to rise since the emojis started
evolving in the first place. Therefore, I wanted to make a some very scaled back
emojis almost to go back to the time when the demand for diversity in
emojis wasn't as prominent. Instead of making the emojis yellow, as they are
traditionally, I chose to make them purple. I did this because I wanted to 
adress the dilemma of color and race. By coloring the emojis purple, I gave 
them an unatural skincolor. This choice was actually inspired by the infamous
"blackface" Pokemon named Jynx, which is a Pokemon that originally had
a black face but was later updated to have a purple face. 


