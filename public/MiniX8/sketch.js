//INDGREDIENTS:
var buttonLeft;
var buttonRight;
var is = 0;

//PREPARATIONS:
function preload(){
  coding = loadJSON("recipe.json");
}

function setup() {
  createCanvas(1250, 550); //Prepare workspace

//Assemble kitchen machine 1
  buttonLeft = createImg('flipLeft.png');
  buttonLeft.size(150, 150);
  buttonLeft.position(0, 400);
  buttonLeft.mousePressed(previousRecipe);

//Assemble kitchen machine 2
  buttonRight = createImg('flipRight.png');
  buttonRight.size(150, 150);
  buttonRight.position(1100, 400);
  buttonRight.mousePressed(nextRecipe);
}

//STEPS:
function draw(){
  if (is >= 8) {
    buttonRight.hide()
  }
  else {
    buttonRight.show()
  }
  if (is <= 0) {
    buttonLeft.hide()
  }
  else {
    buttonLeft.show()
  }
  background ('OldLace'); //Serving plate

  textSize(50); //Seasoning
  textFont('Brush Script MT'); //Seasoning
  fill('SaddleBrown'); //Seasoning
  textAlign(CENTER); //Seasoning
  text('The Recipe Book', width/2, 50); //Seasoning
  textSize(30); //Seasoning
  text('By Line & Laura', width/2, 80); //Element
  text('_________________________', width/2, 90) //Element

  line(440, 160, 440, 400) //Element

  fill(0) //Seasoning
  textSize(50) //Seasoning
  text(coding[is].cooking, width/2, 140) //Element

  textAlign(LEFT); //Seasoning
  textFont('Verdana') //Seasoning
  textSize(12) //Seasoning

//New bowl
  push()
  textStyle(BOLD) //Seasoning
  text('Ingredients:', 30, 180) //Element
  pop()

  for(var j = 0; j < coding[is].writtingARecipe.length; j++) {
    text(coding[is].writtingARecipe[j].forMachines, 30, j*17+200) //Elements
  }

  for(var k = 0; k < coding[is].writtingARecipe.length; k++) {
    text(coding[is].writtingARecipe[k].toCook, 170, k*17+200) //Elements
  }

//New bowl
  push()
  textStyle(BOLD) //Seasoning
  text('Steps:', 450, 180) //Seasoning
  pop()

  for(var l = 0; l < coding[is].aLanguage.length; l++) {
    text("- " + coding[is].aLanguage[l], 450, l*17+200) //Element
  }

//New bowl
  push()
  let pageNo = is + 1 //Conversion of measurements
  textAlign(CENTER) //Seasoning
  textStyle(BOLD) //Seasoning
  textSize(16) //Seasoning
  text(pageNo + " of 9", width/2, 500) //Element
  pop()
}

//PARTS FOR KITCHEN TOOLS
function nextRecipe() {
    is = is + 1
}

function previousRecipe() {
  is = is - 1
}
