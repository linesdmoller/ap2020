![Screenshot](MiniX1Screenshot.png)

RUNME:
[link](https://linesdmoller.gitlab.io/ap2020/MiniX1)

About my project:
To start of with, I decided to learn how to ad photos in code. As a result, I ended up choosing a photo i had drawn of my new sofa. After doing this,
I wanted to explore how to make my program interactable or dynamic in some way. This inspired my concept; I wanted to insert a photo of Thumper the rabbit from "Bambi"
(Thumper's name in Danish is "Stampe" which is part of my middle name and also my nickname) and then I would make Thumper jump in the sofa. At first, I wanted
to make Thumper jump once everytime I pressed "space" on the keyboard. This ended up being too ambitious of a task. Therefore, I chose to make a slider to controle 
the jumping instead.


- How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?

My first independent coding experience has overall been a great experience! Off course it was a bit frustrating when the code didn't work, but when the code 
accually worked, it was a great feeling! I realized that copying code from the 'references' page on p5js.org was not simply a matter of 
copying and pasting - I had to understand the code in order to modify it and make it work as i intended it to. For the most part this was not too big of an 
obstacle though. I am still struggling to undderstand how to put lines of code in the right order.

- How is the coding process different from, or similar to, reading and writing text?

Coding makes sence if you know how to read it - it is like a language. I feel like reading code is very similar to reading math, especially when it comes
to reading/writing coordinates in code. Essentially, every line of code is an order for the program to do something. So when you know how to read those "orders", 
understanding how the program responds to them is very straight forward.

- What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?

I was very excited to learn aboit code and programing. It combines something technical and mathematical with something artistic - both of which are great
interests of mine. This was further emphasized in the assigned readings for this week which made me even more interested to learn how to code with a specific
purpose in mind. 
