  let sofa;
  let thumper;
  let slider;
  function preload() {
    sofa = loadImage("./data/sofa.png");
    thumper = loadImage("./data/thumper.png");
  }
  function setup() {
    createCanvas(1300,550);
    slider = createSlider(30, 270, 100);
  slider.position(10, 10);
  slider.style('width', '80px');
  }
  function draw () {
      let c;
noStroke(); // Don't draw a stroke around shapes
c = color('rgb(38,61,15)');
fill(c); // Use 'c' as fill color
    background(c);
        image(sofa, 250, 200, 700, 495);
    // Here, we use a callback to display the image after loading
        image(thumper, 510, slider.value(), 150, 107);
}
